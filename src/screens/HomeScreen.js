import React from 'react';

import colors from '../config/colors'
import stationIcon from '../assets/stationIcon.png'

import { StyleSheet, Button, View, Text, ActivityIndicator, Alert, Image } from 'react-native';
import RBSheet from "react-native-raw-bottom-sheet";
import MapView, { Marker, Callout, MapViewAnimated } from 'react-native-maps'
import BottomSheet from 'reanimated-bottom-sheet'


import Icon from 'react-native-vector-icons/Ionicons'
import { TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { StationDetails } from '../components/stationDetails'
import api from '../components/api';



export class HomeScreen extends React.Component {
  state = {
    //Map Info
    markers: [],
    region: {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0.001,
      longitudeDelta: 0.001,
    },
    fetchLoading: true,
    gpsLoading: true,
    modalVisible: true,

    selectedStation_id: 0,
    selectedStationData:{},

    //Chose time (fix bug for jumping station details window)
    choseWhen:false,
  }
  setDetailsModalVisible(visible) {
    this.setState({ modalVisible: visible })
  }

  getUser(id) {
    let Id = parseInt(id);
  }
  setRegionByGps() {
    //Get current Geolocation.
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: this.state.region.latitudeDelta,
            longitudeDelta: this.state.region.longitudeDelta
          },
          gpsLoading: false
        })
        return true
      },
      err => {
        //Show alert
        console.error(err)
        Alert.alert("GPS Error", "Could not find your current GPS location.  //" + err)
        return false
      },
      {
        enableHighAccuracy: false
      }
    )
  }
  componentDidMount() {
    //Get marker data from server
    api.get("/stations/")
      .then((res) => {
        this.setState({
          fetchLoading: false,
          markers: res.data
        })
      })
      .catch((e) => {
        console.log(e)
      })
    this.setState({ gpsLoading: true });
    this.setRegionByGps();

  }
  
  
    
      renderInner= () => {
        
        if(this.state.selectedStation_id!=0) return(
        <View style={styles.panel}>
          <Text style={styles.panelTitle}>{this.state.selectedStationData.name}</Text>
          <Text style={styles.panelSubtitle}>
          {this.state.selectedStationData.address}
          </Text>
          <StationDetails currentStation={this.state.selectedStationData} station_id={this.state.selectedStation_id} choseTime={() => {
            if (!this.state.choseWhen) {
              this.bs.current.snapTo(0);
              this.setState({ choseWhen: true })
            }
          }}/>
        </View>
        )
        }
    
  renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  )

  bs = React.createRef()
  
  render() {

    if (this.state.gpsLoading || this.state.fetchLoading) {
      return (
        <ActivityIndicator size="large"></ActivityIndicator>
      )
    } else {
      return (
        <View style={styles.container}>

          <MapView style={styles.map}
            initialRegion={this.state.region}
            ref={(ref) => this.mapView = ref}>

            {this.state.markers.map((marker, i) => (
              <Marker
                ref={_marker => { this.marker = _marker }}
                key={i}
                coordinate={{ latitude: parseFloat(marker.lat), longitude: parseFloat(marker.lng) }}
                title=""
                onPress={e => {
                  this.setState({
                    selectedStation_id: marker.id,
                    selectedStationData:marker
                  })
                  this.bs.current.snapTo(1)
                  this.mapView.animateCamera({
                    center:{ ...this.state.region,latitude:this.state.region.latitude-0.0004}
                  },{
                    duration:0
                  })
                  console.log(this.state.region)
                }}>
                <Image source={stationIcon} style={{ width: 47, height: 47 }} />
                <Callout style={{width:170}}>
                  <Text style={{color:colors.black,fontSize:20}}>{marker.name}</Text>
                  <Text style={{color:colors.gray,fontSize:15}}>{marker.address}</Text>

                </Callout>
              </Marker>
            ))}
          </MapView>
          <TouchableOpacity
            onPress={() => {
              this.setState({
                gpsLoading: true
              })
              this.setRegionByGps()
            }}>
            <View style={{ backgroundColor: 'rgba(255,255,255,0.9)', height: 75, width: 75, borderRadius: 150, alignItems: 'center', margin: 7 }}>
              <Icon name="md-locate" size={45} color={colors.tintInactive} style={{ marginTop: 15, alignSelf: 'center' }} />
            </View>
          </TouchableOpacity>
          <BottomSheet
            ref={this.bs}
            snapPoints={[500, 250, 0]}
            renderContent={this.renderInner}
            renderHeader={this.renderHeader}
            initialSnap={2}
          
          />
        </View>
      );
    }
  }

}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    alignItems: 'flex-start'
  },
  map: {
    flex: 1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },

  panelContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  panel: {
    height: 600,
    padding: 20,
    backgroundColor: colors.white,
  },
  header: {
    backgroundColor: colors.white,
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: colors.whitishgray,
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 30,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 16,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  photo: {
    width: '100%',
    height: 225,
    marginTop: 30,
  },
})
