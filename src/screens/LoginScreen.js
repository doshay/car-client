import React from 'react';
import { View, TextInput, Text, Button, StyleSheet, ScrollView, Image, AsyncStorage } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json
import { Hideo } from 'react-native-textinput-effects';
import Icons from 'react-native-vector-icons/Ionicons'
import colors from '../config/colors';
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import imgIcon from '../assets/stationIcon.png'
import Swiper from 'react-native-swiper'
export class LoginScreen extends React.Component {

  state = {
    uname: '',
    pass: '',
    fontLoaded: false,
    currentScreen:0
  }



  async login() {
    try{
      await AsyncStorage.setItem('@User_id','1')
    }catch(e){
      console.log(e)
    }
    this.props.navigation.navigate('Main')
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{ flex: 1, padding: 20, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontFamily: 'DevanagariSangamMN-Bold', marginTop: 30, fontSize: 32, color: colors.tintActive }}>{(this.state.currentScreen == 0) ? 'Own A Station' : 'Find A Station'}</Text>
        </View>
        <View style={{ flex: 4, paddingHorizontal: 5, borderWidth: 0.7, borderRadius: 20, borderColor: colors.lightgray, marginHorizontal: 19 }}>
          <Swiper onIndexChanged={(e) => this.setState({currentScreen:e})} prevButton={<Text></Text>} nextButton={<Text></Text>}
            activeDotColor={colors.tintActive}
            dotColor={colors.lightgray}

            showsButtons={true}>
            <View style={[styles.slide1, { marginTop: 70 }]}>
              <Text style={{ fontSize: 27, color: colors.tintInactive, fontFamily: 'DevanagariSangamMN-Bold', justifyContent: 'center', textAlign: 'center', marginBottom: 10 }}>
                Start Earning.
              </Text>
              <Text style={{ fontSize: 20, color: colors.lightishgray, fontFamily: 'DevanagariSangamMN', justifyContent: 'center', textAlign: 'center' }}>
                Set up your own station and earn money by letting passers use it.
              </Text>
              <TouchableOpacity activeOpacity={0.55} style={{
                marginTop: 70,
                height: 50,
                width: 230,
                borderRadius: 200,
                backgroundColor: colors.tintActive,
                justifyContent: 'center',
                alignSelf: 'center'
              }}>
                <Text style={{ textAlign: 'center', color: colors.white, fontFamily: 'DevanagariSangamMN-Bold', marginTop: 3, justifyContent: 'center', fontSize: 20 }}>Get Started</Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.slide1, { marginTop: 70 }]}>
            <Text style={{ fontSize: 27, color: colors.tintInactive, fontFamily: 'DevanagariSangamMN-Bold', justifyContent: 'center', textAlign: 'center', marginBottom: 10 }}>
                Start Saving.
              </Text>
              <Text style={{ fontSize: 20, color: colors.lightishgray, fontFamily: 'DevanagariSangamMN', justifyContent: 'center', textAlign: 'center' }}>
                Find nearby stations for your electric car quickly.
              </Text>
              <TouchableOpacity activeOpacity={0.55} style={{
                marginTop: 70,
                height: 50,
                width: 230,
                borderRadius: 200,
                backgroundColor: colors.tintActive,
                justifyContent: 'center',
                alignSelf: 'center'
              }}>
                <Text style={{ textAlign: 'center', color: colors.white, fontFamily: 'DevanagariSangamMN-Bold', marginTop: 3, justifyContent: 'center', fontSize: 20 }}>Let's Go</Text>
              </TouchableOpacity>
            </View>

          </Swiper>


        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Text style={{ color: colors.lightgray, fontSize: 16, textAlign: 'center' }}>Already Have an account?</Text>
          <Button onPress={() => this.login()} title="Sign In" color={colors.tintActive} />
        </View>
      </View>
    );

  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  content: {
    // not cool but good enough to make all inputs visible when keyboard is active
    paddingBottom: 300,
  },
  card1: {
    paddingVertical: 16,
  },
  card2: {
    padding: 16,
  },
  input: {
    marginTop: 4,
  },
  title: {
    paddingBottom: 16,
    textAlign: 'center',
    color: '#404d5b',
    fontSize: 20,
    fontWeight: 'bold',
    opacity: 0.8,
  },
  wrapper: {},
  slide1: {
    flex: 1,
    alignItems: 'center',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  }
});
