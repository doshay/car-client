const colors = {
    
    
    black: '#010001',
    night: '#333333',
    charcoal: '#474747',
    gray: '#7D7D7D',
    lightishgray: '#9D9D9D',
    lightgray: '#D6D6D6',
    whitishgray:'#f2f2f2',
    smoke: '#EEEEEE',
    white: '#ffffff',
    ypsDark: '#47546E',
    yps: '#637599',
    ypsLight: '#7B92BF',
    cosmic: '#963D32',

    
    //@Texts
    mutedText:'#999999',

    //@Tints custom
    tintActive:'#FF6F00', 
    tintInactive:'#263238',

    //@BG - Active Colors
    bgLight: '#ff8b33',

    //@Icons
    iconActive: '#211717'

   
  };
 export default colors;