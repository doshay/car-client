import React, { Component } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import {HourBtn} from '../components/hourbtn'
import { StyleSheet, Button, View, Text, ActivityIndicator, Alert, DatePickerIOS, TouchableWithoutFeedback, TouchableHighlight } from 'react-native';
import colors from '../config/colors'
import { sanFranciscoWeights } from 'react-native-typography'

import Slider from 'react-native-slider-custom';
import api from './api';
import moment from 'moment';

export class StationDetails extends Component {

    state = {
        timeChosen: new Date(),
        userChoseTime: false, //Use to determinte if user chose time
        currentStation_id: this.props.station_id,
        selectedHourIdx:null,
        currentStation: this.props.currentStation,
        hours:[
            '10:50',
            '12:30',
            '13:45',
            '14:00'
        ],
        sliderStep:0.5,
        fetchLoading: true,
        //Hours manage
        
        
    }
    componentDidMount() {
        this.loadData()
    }

     IsTimeNow__(date){ //Check if time user chose is current time
        if(moment(date).format("hh/mm") == moment(this.state.timeChosen).format("hh/mm"))
            return true
        else
           return false 
    }
   
    loadData() { 
        console.log(this.state.currentStation_id)
        //Start request from server
       api.get("/stations/" + this.state.currentStation_id +"/wtime/")
            .then((res) => {
                console.log(res.data)
                let fetch = res.data.map(hour => {
                    return moment(hour.preftime,"HH:mm:ss").format('HH:mm') 
                })
                console.log(fetch)
                
                this.setState({ hours: fetch, fetchLoading: false })
            })
            .catch((e)=>{e})
    }
    
    render() {
        
        if (this.state.fetchLoading) {
            return <ActivityIndicator />
        } else {
            var t = this;
            return (
                <View style={{ flex: 1, margin: 5, justifyContent: 'flex-start' }}>

                    <View style={{ flex: 1, flexDirection: 'column'}}>
                        <Text style={[sanFranciscoWeights.regular, { marginBottom: 9, fontSize: 20 }]}>When</Text>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
                            
                            {this.state.hours.map(function (hour, index) {
                                return (
                                    <HourBtn hour={hour} key={index} selected={index === t.state.selectedHourIdx} onSelect={() => {
                                        t.setState({ selectedHourIdx: index })
                                        t.props.choseTime()
                                    }} />
                                )
                            })}
                        </ScrollView>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column' ,marginTop:20}}>
                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={[sanFranciscoWeights.regular, { flex: 1, marginBottom: 9, fontSize: 20 }]}>Duration</Text>
                            <Text style={[sanFranciscoWeights.semibold, { flex: 1, marginBottom: 9, fontSize: 14, textAlign: 'right', color: colors.tintInactive }]}>{moment.duration(this.state.sliderStep, 'hours').humanize()}</Text>

                        </View>
                        <Slider
                            minimumValue={0.5}
                            maximumValue={8}
                            step={0.5}
                            trackStyle={{ height: 6}}
                            value={this.state.sliderStep}
                            onValueChange={value => this.setState({ sliderStep: value })}
                        />
                    </View>
                    <View style={{ flex: 3 ,justifyContent:'center',marginBottom:50}}>
                        <TouchableWithoutFeedback>
                            <View style={styles.orderBtn}>
                                <Text style={[styles.btnText,sanFranciscoWeights.regular]}>Book a charge</Text>
                            </View>
                        </TouchableWithoutFeedback>

                    </View>
                </View>
            )
        }
    }
}
const styles = StyleSheet.create({

    timeBtn: {
        flexDirection: 'row-reverse',
        alignSelf: 'center',
        justifyContent: 'center',
        height: 50, width: 200,
        borderColor: colors.tintActive,
        borderRadius: 350,
        paddingTop: 3.8,
    },
    btnText:{
        color:colors.white,
        fontSize:17,
        marginBottom:3
    },
    iconCircle:{
        borderWidth:3.2,
        borderColor:colors.black,
        alignItems:'center',
        justifyContent:'center',
        width:53,
        height:53,
        backgroundColor:'#fff',
        borderRadius:50
    },
    orderBtn:{
        flexDirection: 'row-reverse',
        alignItems: 'center',
        alignSelf:'center',
        justifyContent: 'center',
        height: 50, width: 260,
        backgroundColor: colors.tintActive,
        borderRadius: 10,
        paddingTop: 3.8,
    }

})