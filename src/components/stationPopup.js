import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback,StyleSheet } from 'react-native';
import colors from '../config/colors'
import { sanFranciscoWeights } from 'react-native-typography'

export class StationPopup extends Component {
    
  state = {
    choseWhen:false,
  } 
  
  render(){
        return(
            <View style={styles.panel}>
            <Text style={styles.panelTitle}>San Francisco Airport</Text>
            <Text style={styles.panelSubtitle}>
              International Airport - 40 miles away
            </Text>
            <StationDetails id={1} choseTime={() => {
              if (!this.state.choseWhen) {
                this.bs.current.snapTo(0);
                this.setState({ choseWhen: true })
              }
            }}/>
          </View>
        )
     
        
    }
}

const styles = StyleSheet.create({
    panel: {
        height: 600,
        padding: 20,
        backgroundColor: colors.white,
      },
      panelTitle: {
        fontSize: 30,
        height: 35,
      },
      panelSubtitle: {
        fontSize: 16,
        color: 'gray',
        height: 30,
        marginBottom: 10,
      }
})