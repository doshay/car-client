import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback,StyleSheet } from 'react-native';
import colors from '../config/colors'
import { sanFranciscoWeights } from 'react-native-typography'

export class HourBtn extends Component {
    render(){
        return(
            <TouchableWithoutFeedback
            onPress={() => this.props.onSelect()}
            >
            <View style={this.props.selected ? styles.nonactive : styles.active}>
            <Text style={[sanFranciscoWeights.light,{fontSize:16,textAlign:'center',color:this.props.selected ? colors.whitishgray : colors.tintInactive}]}>{this.props.hour}</Text>
            </View>
            </TouchableWithoutFeedback>
            
        )
     
        
    }
}

const styles = StyleSheet.create({
    nonactive:{
        width:85,
        height:42,
        backgroundColor:colors.tintInactive,
        color:colors.white,
        borderRadius:3.5,
        justifyContent:'center',
        marginRight:10,
    },
    active:{
        width:85,
        height:42,
        backgroundColor:colors.whitishgray,
        borderRadius:3.5,
        justifyContent:'center',
        marginRight:10,
    }
})