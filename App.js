import React from 'react';
import {Platform, StyleSheet, Text, View,Button} from 'react-native';

import  colors  from './src/config/colors'
import { createAppContainer,createSwitchNavigator } from 'react-navigation'; // Version can be specified in package.json
import {createDrawerNavigator, DrawerItems} from 'react-navigation-drawer'
import {createBottomTabNavigator} from 'react-navigation-tabs'
import {createStackNavigator} from 'react-navigation-stack'
import { HomeScreen } from './src/screens/HomeScreen'
import { DetailsScreen } from './src/screens/DetailsScreen'
import Icon from 'react-native-vector-icons/Ionicons'
import { YellowBox } from 'react-native'; //ignore errors about debugger.
import { TouchableHighlight } from 'react-native-gesture-handler';
import {LoginScreen} from './src/screens/LoginScreen'

console.disableYellowBox = true;
YellowBox.ignoreWarnings(['Remote debugger']);


state={

}

const HomeStack = createStackNavigator({
  Home: {screen:HomeScreen,
    
    navigationOptions: ({ navigation }) => ({
      title: `Home`,
      headerRight:  <Icon style={{marginRight:20}} name='md-menu' size={30} onPress={()=>{navigation.openDrawer()}}/>,
      headerStyle:{
        textAlign:'center',
        height:60,
        borderColor:'#ffffff',
        alignContent: 'center',
        borderBottomWidth: 0,
      }
    }),
  },
  Details:{screen: DetailsScreen,
    navigationOptions: ({ navigation }) => ({
      title: `Details`,
      //headerLeft:  <Icon name='md-menu' size={30}/>,
      headerStyle:{
        alignContent: 'center',
      }
    }),
  },
},{
   initialRouteName: 'Home',

}

);

const bottomTabNavigator = createBottomTabNavigator(
  {
    Home: HomeStack,
    Settings: DetailsScreen,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === 'Home') {
          return (
           <Icon name="md-pin" size={30} color={tintColor}/>
          );
        } else {
          return (
            <Icon name="md-settings" size={30} color={tintColor}/>
          );
        }
      },
    }),
    tabBarOptions: {
      showLabel:false,
      activeTintColor: '#FF6F00',
      inactiveTintColor: '#263238',
      style:{
        borderTopColor:'#ffffff',
        height:60
      }
    },
  }

)

const DrawerContent = props => (
  <View style={{textAlign:'right'}}>
    <View
      style={{
        backgroundColor: colors.tintActive,
        height: 140,
        alignItems: 'center',
        justifyContent: 'center',

      }}
    >
     <Icon name="md-person" style={{marginTop:18}} color={colors.white} size={45}/>
     <Text style={{color:colors.white,fontSize:23}}>שי ליבני</Text>
    </View>
    <DrawerItems  {...props} />
  </View>
)
const dashboardStack = createDrawerNavigator({
  'דף הבית': bottomTabNavigator,

},{
  contentOptions:{
    
    labelStyle:{
      fontSize:20,
      color:colors.tintInactive,
    },
    itemStyle:{
      flexDirection:'right'
    }
    
  },
  drawerPosition:'right'
  
})
const authStack = createSwitchNavigator({
  //Login:LoginScreen,
  Main:dashboardStack

})
export default createAppContainer(authStack) 
